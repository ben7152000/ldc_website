import { filename } from 'pathe/utils'

export default function useAssets(path: string) {
  let assets

  if (/(\.svg)$/i.exec(path)) {
    assets = import.meta.glob('~/public/images/**/*.svg', { eager: true })
  } else if (/(\.png)$/i.exec(path)) {
    assets = import.meta.glob('~/public/images/**/*.png', { eager: true })
  } else {
    assets = import.meta.glob('~/public/images/**/*.jpg', { eager: true })
  }

  const fileName = filename(path)

  const images = Object.fromEntries(
    Object.entries(assets).map(([key, value]) => [
      filename(key),
      (value as Record<string, any>).default,
    ])
  )

  return images[fileName].replace('_nuxt/public', '')
}
