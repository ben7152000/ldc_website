export const setSeoMeta = (page) => {
  const { title = '', description = '' } = page
  const pageImage = page.img ? `_nuxt/assets/image/${page.img}` : '_nuxt/assets/image/about-1.jpg'

  return {
    title,
    ogTitle: title,
    description,
    ogDescription: description,
    ogImage: pageImage,
  }
}

export const getCourseList = ({ contentQuery, path }) => {
  return (
    contentQuery
      .filter((c) => c._path !== path)
      .map((item) => ({
        title: item?.title || '',
        description: item?.description || '',
        src: item?.img || '',
        alt: item?.alt || '',
        path: item?._path || '',
      })) || []
  )
}
