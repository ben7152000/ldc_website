import { buildVersion } from './version'

export function getVersion() {
  const date = new Date(buildVersion)
  return date.toLocaleString('en-US', { timeZone: 'Asia/Taipei' })
}
