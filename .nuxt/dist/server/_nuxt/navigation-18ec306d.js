import { hash } from "ohash";
import { b as useRuntimeConfig } from "../server.mjs";
import "vue";
import "destr";
import "devalue";
import "klona";
import { q as queryContent, e as encodeQueryParams, j as jsonStringify } from "./query-003efbbe.js";
import { w as withContentBase, a as addPrerenderPath, s as shouldUseClientDB } from "./utils-c338605d.js";
import { u as useContentPreview } from "./preview-c5948799.js";
const fetchContentNavigation = async (queryBuilder) => {
  const { content } = useRuntimeConfig().public;
  if (typeof (queryBuilder == null ? void 0 : queryBuilder.params) !== "function") {
    queryBuilder = queryContent(queryBuilder);
  }
  const params = queryBuilder.params();
  const apiPath = content.experimental.stripQueryParameters ? withContentBase(`/navigation/${`${hash(params)}.${content.integrity}`}/${encodeQueryParams(params)}.json`) : withContentBase(`/navigation/${hash(params)}.${content.integrity}.json`);
  {
    addPrerenderPath(apiPath);
  }
  if (shouldUseClientDB()) {
    const generateNavigation = await import("./client-db-f34fa842.js").then((m) => m.generateNavigation);
    return generateNavigation(params);
  }
  const data = await $fetch(apiPath, {
    method: "GET",
    responseType: "json",
    params: content.experimental.stripQueryParameters ? void 0 : {
      _params: jsonStringify(params),
      previewToken: useContentPreview().getPreviewToken()
    }
  });
  if (typeof data === "string" && data.startsWith("<!DOCTYPE html>")) {
    throw new Error("Not found");
  }
  return data;
};
export {
  fetchContentNavigation as f
};
//# sourceMappingURL=navigation-18ec306d.js.map
