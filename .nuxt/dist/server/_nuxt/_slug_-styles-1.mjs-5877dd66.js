const AlbumEquipment_vue_vue_type_style_index_0_scoped_8ff1330f_lang = "@media (min-width:768px){.grid-album[data-v-8ff1330f]{grid-auto-rows:minmax(100px,125px);grid-template-columns:repeat(auto-fit,minmax(330px,1fr))}}[data-v-8ff1330f]::-webkit-scrollbar{width:6px}[data-v-8ff1330f]::-webkit-scrollbar-thumb{background:#34b1b1;border-radius:4px}[data-v-8ff1330f]::-webkit-scrollbar-thumb:hover{background:#2f9e9e}";
export {
  AlbumEquipment_vue_vue_type_style_index_0_scoped_8ff1330f_lang as default
};
//# sourceMappingURL=_slug_-styles-1.mjs-5877dd66.js.map
