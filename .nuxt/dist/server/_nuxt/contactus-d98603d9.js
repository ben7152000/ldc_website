import { _ as __nuxt_component_5 } from "./IndexContact-701035d9.js";
import { _ as __nuxt_component_2 } from "./nuxt-img-c5557f00.js";
import { mergeProps, unref, useSSRContext } from "vue";
import { ssrRenderAttrs, ssrRenderComponent } from "vue/server-renderer";
import { S as SEO_DESCRIPTION } from "./index-ab0f4448.js";
import "./Icon-78f87aca.js";
import "../server.mjs";
import "ofetch";
import "#internal/nitro";
import "hookable";
import "unctx";
import "vue-router";
import "h3";
import "ufo";
import "devalue";
import "@unhead/ssr";
import "unhead";
import "@unhead/shared";
import "destr";
import "klona";
import "defu";
import "./config-6f8e3cb8.js";
import "@iconify/vue/dist/offline";
import "@iconify/vue";
import "./_plugin-vue_export-helper-cc2b3d55.js";
import "./ssr-cd1cbb71.js";
const _sfc_main = {
  __name: "contactus",
  __ssrInlineRender: true,
  setup(__props) {
    return (_ctx, _push, _parent, _attrs) => {
      const _component_IndexContact = __nuxt_component_5;
      const _component_nuxt_img = __nuxt_component_2;
      _push(`<div${ssrRenderAttrs(mergeProps({ class: "relative md:flex md:h-[100vh] md:items-center" }, _attrs))}>`);
      _push(ssrRenderComponent(_component_IndexContact, null, null, _parent));
      _push(ssrRenderComponent(_component_nuxt_img, {
        format: "webp",
        quality: "80",
        src: "/images/subtract-front-2.svg",
        alt: unref(SEO_DESCRIPTION),
        class: "absolute right-0 top-[60px] -z-10 hidden opacity-80 md:inline-block"
      }, null, _parent));
      _push(ssrRenderComponent(_component_nuxt_img, {
        format: "webp",
        quality: "80",
        src: "/images/subtract-back-2.svg",
        alt: unref(SEO_DESCRIPTION),
        class: "absolute bottom-0 left-0 -z-10 hidden opacity-80 md:inline-block"
      }, null, _parent));
      _push(`</div>`);
    };
  }
};
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("pages/contactus.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
export {
  _sfc_main as default
};
//# sourceMappingURL=contactus-d98603d9.js.map
