import { _ as __nuxt_component_2 } from "./nuxt-img-c5557f00.js";
import { mergeProps, unref, useSSRContext } from "vue";
import { ssrRenderAttrs, ssrRenderComponent } from "vue/server-renderer";
import { S as SEO_DESCRIPTION } from "./index-ab0f4448.js";
import { u as useAssets } from "./useAssets-ef8ea7b3.js";
import "../server.mjs";
import "ofetch";
import "#internal/nitro";
import "hookable";
import "unctx";
import "vue-router";
import "h3";
import "ufo";
import "devalue";
import "@unhead/ssr";
import "unhead";
import "@unhead/shared";
import "destr";
import "klona";
import "defu";
import "./ssr-cd1cbb71.js";
import "pathe/utils";
const _sfc_main = {
  __name: "DetailImage",
  __ssrInlineRender: true,
  props: {
    src: {
      type: String,
      required: true
    },
    alt: {
      type: String,
      required: true
    }
  },
  setup(__props) {
    const altDescription = (alt) => {
      return `${alt},${SEO_DESCRIPTION}`;
    };
    return (_ctx, _push, _parent, _attrs) => {
      const _component_nuxt_img = __nuxt_component_2;
      _push(`<div${ssrRenderAttrs(mergeProps({ class: "h-full w-full flex-auto overflow-hidden rounded-tl-60" }, _attrs))}>`);
      _push(ssrRenderComponent(_component_nuxt_img, {
        format: "webp",
        quality: "80",
        src: unref(useAssets)(__props.src),
        alt: altDescription(__props.alt),
        sizes: "sm:300px md:350px lg:800px",
        class: "h-full w-full object-cover object-center",
        loading: "lazy"
      }, null, _parent));
      _push(`</div>`);
    };
  }
};
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("components/content/DetailImage.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
export {
  _sfc_main as default
};
//# sourceMappingURL=DetailImage-3065380e.js.map
