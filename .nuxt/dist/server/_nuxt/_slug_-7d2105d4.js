import { s as setSeoMeta, _ as __nuxt_component_0 } from "./common-6de92c75.js";
import _sfc_main$1 from "./ContentDoc-d4bdbd8b.js";
import { withAsyncContext, mergeProps, useSSRContext } from "vue";
import "hookable";
import { f as useRoute, g as useSeoMeta } from "../server.mjs";
import { u as useAsyncData, q as queryContent } from "./query-003efbbe.js";
import "destr";
import "devalue";
import "klona";
import { ssrRenderAttrs, ssrRenderComponent } from "vue/server-renderer";
import { P as PAGE_PATH } from "./index-ab0f4448.js";
import "./nuxt-link-744fccd2.js";
import "ufo";
import "./Icon-78f87aca.js";
import "./config-6f8e3cb8.js";
import "@iconify/vue/dist/offline";
import "@iconify/vue";
import "./_plugin-vue_export-helper-cc2b3d55.js";
import "ofetch";
import "#internal/nitro";
import "unctx";
import "vue-router";
import "h3";
import "@unhead/ssr";
import "unhead";
import "@unhead/shared";
import "defu";
import "./navigation-18ec306d.js";
import "ohash";
import "./utils-c338605d.js";
import "./ssr-cd1cbb71.js";
import "./preview-c5948799.js";
import "cookie-es";
import "./ContentRenderer-9978ff9b.js";
import "./ContentRendererMarkdown-0d6708b2.js";
import "scule";
import "property-information";
import "./ContentQuery-9e1ec63f.js";
const _sfc_main = {
  __name: "[slug]",
  __ssrInlineRender: true,
  async setup(__props) {
    let __temp, __restore;
    const { path } = useRoute();
    const { data: page } = ([__temp, __restore] = withAsyncContext(() => useAsyncData(
      `content-${path}`,
      () => queryContent().where({ _path: path }).only(PAGE_PATH).findOne()
    )), __temp = await __temp, __restore(), __temp);
    useSeoMeta({ ...setSeoMeta(page) });
    return (_ctx, _push, _parent, _attrs) => {
      const _component_BreadcrumbNav = __nuxt_component_0;
      const _component_ContentDoc = _sfc_main$1;
      _push(`<div${ssrRenderAttrs(mergeProps({ class: "container mx-auto px-6 pb-24 pt-12" }, _attrs))}>`);
      _push(ssrRenderComponent(_component_BreadcrumbNav, null, null, _parent));
      _push(ssrRenderComponent(_component_ContentDoc, null, null, _parent));
      _push(`</div>`);
    };
  }
};
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("pages/experience/sup/[slug].vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
export {
  _sfc_main as default
};
//# sourceMappingURL=_slug_-7d2105d4.js.map
