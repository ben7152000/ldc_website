import { M as MetaHead, _ as __nuxt_component_0 } from "./ListContainer-7adf6140.js";
import { _ as __nuxt_component_1 } from "./AlbumCourse-5c308264.js";
import { withAsyncContext, computed, unref, withCtx, createVNode, useSSRContext } from "vue";
import "vue-router";
import "hookable";
import { f as useRoute, g as useSeoMeta } from "../server.mjs";
import { u as useAsyncData, q as queryContent } from "./query-003efbbe.js";
import "destr";
import "devalue";
import "klona";
import { ssrRenderAttrs, ssrRenderComponent } from "vue/server-renderer";
import { g as getCourseList, s as setSeoMeta } from "./common-6de92c75.js";
import { P as PAGE_PATH, C as CONTENT_QUERY } from "./index-ab0f4448.js";
import "./nuxt-link-744fccd2.js";
import "ufo";
import "./nuxt-img-c5557f00.js";
import "defu";
import "h3";
import "./ssr-cd1cbb71.js";
import "./navigation-18ec306d.js";
import "ohash";
import "./utils-c338605d.js";
import "./preview-c5948799.js";
import "cookie-es";
import "./_plugin-vue_export-helper-cc2b3d55.js";
import "./HoverImage-812cf7ee.js";
import "./useAssets-ef8ea7b3.js";
import "pathe/utils";
import "ofetch";
import "#internal/nitro";
import "unctx";
import "@unhead/ssr";
import "unhead";
import "@unhead/shared";
import "./Icon-78f87aca.js";
import "./config-6f8e3cb8.js";
import "@iconify/vue/dist/offline";
import "@iconify/vue";
const _sfc_main = {
  __name: "[slug]",
  __ssrInlineRender: true,
  async setup(__props) {
    var _a, _b;
    let __temp, __restore;
    const { path } = useRoute();
    const { data: page } = ([__temp, __restore] = withAsyncContext(() => useAsyncData(
      `content-${path}`,
      () => queryContent().where({ _path: path }).only(PAGE_PATH).findOne()
    )), __temp = await __temp, __restore(), __temp);
    const contentQuery = ([__temp, __restore] = withAsyncContext(() => queryContent(path).only(CONTENT_QUERY).find()), __temp = await __temp, __restore(), __temp);
    const title = ((_a = page.value) == null ? void 0 : _a.title) || "";
    const description = ((_b = page.value) == null ? void 0 : _b.description) || title;
    const childItems = computed(() => getCourseList({ contentQuery, path }));
    useSeoMeta({ ...setSeoMeta(page.value) });
    return (_ctx, _push, _parent, _attrs) => {
      const _component_MetaHead = MetaHead;
      const _component_ListContainer = __nuxt_component_0;
      const _component_AlbumCourse = __nuxt_component_1;
      _push(`<div${ssrRenderAttrs(_attrs)}>`);
      _push(ssrRenderComponent(_component_MetaHead, {
        title: unref(title),
        description: unref(description)
      }, null, _parent));
      _push(ssrRenderComponent(_component_ListContainer, { title: unref(title) }, {
        default: withCtx((_, _push2, _parent2, _scopeId) => {
          if (_push2) {
            _push2(ssrRenderComponent(_component_AlbumCourse, { data: unref(childItems) }, null, _parent2, _scopeId));
          } else {
            return [
              createVNode(_component_AlbumCourse, { data: unref(childItems) }, null, 8, ["data"])
            ];
          }
        }),
        _: 1
      }, _parent));
      _push(`</div>`);
    };
  }
};
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("pages/diving/[slug].vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
export {
  _sfc_main as default
};
//# sourceMappingURL=_slug_-89a42dd1.js.map
