const IndexHero_vue_vue_type_style_index_0_scoped_f924e788_lang = "@media (min-width:768px){.hero-bg[data-v-f924e788]{background-image:url(" + __buildAssetsURL("hero-bg.c591c5f5.png") + ");background-position:top;background-repeat:no-repeat;background-size:cover;border-top-right-radius:80px}}.triangle[data-v-f924e788]{border-color:transparent transparent #34b1b1;border-style:solid;border-width:0 15px 15px;color:#abaaaa;height:0;left:50%;top:100%;transform:rotate(180deg) translateX(50%);width:0}";
export {
  IndexHero_vue_vue_type_style_index_0_scoped_f924e788_lang as default
};
//# sourceMappingURL=index-styles-1.mjs-2f31f7de.js.map
