import { _ as __nuxt_component_0 } from './HoverImage-812cf7ee.mjs';
import { useSSRContext, defineComponent, mergeProps } from 'file:///Users/apple/Desktop/software/website/LDC_website/node_modules/vue/index.mjs';
import { ssrRenderAttrs, ssrRenderList, ssrRenderComponent } from 'file:///Users/apple/Desktop/software/website/LDC_website/node_modules/vue/server-renderer/index.mjs';
import { _ as _export_sfc } from './_plugin-vue_export-helper-cc2b3d55.mjs';

const _sfc_main = /* @__PURE__ */ defineComponent({
  __name: "AlbumCourse",
  __ssrInlineRender: true,
  props: {
    data: {
      type: Array,
      required: true
    }
  },
  setup(__props) {
    return (_ctx, _push, _parent, _attrs) => {
      const _component_HoverImage = __nuxt_component_0;
      _push(`<div${ssrRenderAttrs(mergeProps({ class: "mt-6 min-h-[75vh] overflow-y-auto pr-2 pt-2 md:h-[61vh]" }, _attrs))} data-v-bdf6329a><div class="grid grid-cols-6 gap-2 md:gap-3" data-v-bdf6329a><!--[-->`);
      ssrRenderList(__props.data, (course, i) => {
        _push(`<div class="col-span-full h-[240px] min-w-[240px] overflow-hidden rounded-xl sm:col-span-3 lg:col-span-2" data-v-bdf6329a>`);
        _push(ssrRenderComponent(_component_HoverImage, mergeProps(course, { "hover-background-style": "bg-secondary" }), null, _parent));
        _push(`</div>`);
      });
      _push(`<!--]--></div></div>`);
    };
  }
});
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("components/AlbumCourse.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const __nuxt_component_1 = /* @__PURE__ */ _export_sfc(_sfc_main, [["__scopeId", "data-v-bdf6329a"]]);

export { __nuxt_component_1 as _ };
//# sourceMappingURL=AlbumCourse-5c308264.mjs.map
