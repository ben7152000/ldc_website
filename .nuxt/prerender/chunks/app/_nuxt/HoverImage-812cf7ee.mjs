import { _ as __nuxt_component_0$1 } from './nuxt-link-744fccd2.mjs';
import { _ as __nuxt_component_2 } from './nuxt-img-c5557f00.mjs';
import { useSSRContext, computed, mergeProps, withCtx, unref, createVNode, toDisplayString, openBlock, createBlock, createCommentVNode } from 'file:///Users/apple/Desktop/software/website/LDC_website/node_modules/vue/index.mjs';
import { ssrRenderAttrs, ssrRenderComponent, ssrRenderClass, ssrInterpolate } from 'file:///Users/apple/Desktop/software/website/LDC_website/node_modules/vue/server-renderer/index.mjs';
import { u as useAssets } from './useAssets-ef8ea7b3.mjs';
import { S as SEO_DESCRIPTION } from './index-ab0f4448.mjs';

const _sfc_main = {
  __name: "HoverImage",
  __ssrInlineRender: true,
  props: {
    src: {
      type: String,
      required: true
    },
    alt: {
      type: String,
      required: true
    },
    title: {
      type: String,
      required: true
    },
    description: {
      type: String,
      default: ""
    },
    path: {
      type: String,
      required: true
    },
    hoverBackgroundStyle: {
      type: String,
      default: "bg-main-bg-white/90"
    }
  },
  setup(__props) {
    const props = __props;
    const descriptionText = computed(() => {
      if (props.description.length > 20) {
        return `${props.description.slice(0, 30)}...`;
      }
      return props.description;
    });
    const altDescription = (alt) => {
      return `${alt},${SEO_DESCRIPTION}`;
    };
    return (_ctx, _push, _parent, _attrs) => {
      const _component_NuxtLink = __nuxt_component_0$1;
      const _component_nuxt_img = __nuxt_component_2;
      _push(`<div${ssrRenderAttrs(mergeProps({ class: "group relative h-full w-full" }, _attrs))}>`);
      _push(ssrRenderComponent(_component_NuxtLink, {
        to: { path: __props.path, query: { tab: "detail" } }
      }, {
        default: withCtx((_, _push2, _parent2, _scopeId) => {
          if (_push2) {
            _push2(ssrRenderComponent(_component_nuxt_img, {
              format: "webp",
              quality: "80",
              src: unref(useAssets)(__props.src),
              sizes: "sm:100vw md:50vw lg:400px",
              alt: altDescription(__props.alt),
              class: "h-full w-full object-cover object-center",
              loading: "lazy"
            }, null, _parent2, _scopeId));
            _push2(`<div class="${ssrRenderClass(`absolute bottom-0 left-0 top-0 hidden h-full w-full cursor-pointer flex-col items-center justify-center lg:flex ${__props.hoverBackgroundStyle} p-4 opacity-0 transition-opacity duration-500 group-hover:opacity-95`)}"${_scopeId}><h4 class="text-center text-base font-semibold text-main-bg-dark"${_scopeId}>${ssrInterpolate(__props.title)}</h4>`);
            if (unref(descriptionText)) {
              _push2(`<p class="mt-4 text-center text-base"${_scopeId}>${ssrInterpolate(unref(descriptionText))}</p>`);
            } else {
              _push2(`<!---->`);
            }
            _push2(`</div><div class="absolute bottom-0 right-0 z-10 rounded-bl-sm rounded-tl-xl bg-primary/80 p-4 text-center md:px-3 md:py-2 lg:hidden"${_scopeId}><p class="mb-0 text-main-bg-white"${_scopeId}>${ssrInterpolate(__props.title)}</p></div>`);
          } else {
            return [
              createVNode(_component_nuxt_img, {
                format: "webp",
                quality: "80",
                src: unref(useAssets)(__props.src),
                sizes: "sm:100vw md:50vw lg:400px",
                alt: altDescription(__props.alt),
                class: "h-full w-full object-cover object-center",
                loading: "lazy"
              }, null, 8, ["src", "alt"]),
              createVNode("div", {
                class: `absolute bottom-0 left-0 top-0 hidden h-full w-full cursor-pointer flex-col items-center justify-center lg:flex ${__props.hoverBackgroundStyle} p-4 opacity-0 transition-opacity duration-500 group-hover:opacity-95`
              }, [
                createVNode("h4", { class: "text-center text-base font-semibold text-main-bg-dark" }, toDisplayString(__props.title), 1),
                unref(descriptionText) ? (openBlock(), createBlock("p", {
                  key: 0,
                  class: "mt-4 text-center text-base"
                }, toDisplayString(unref(descriptionText)), 1)) : createCommentVNode("", true)
              ], 2),
              createVNode("div", { class: "absolute bottom-0 right-0 z-10 rounded-bl-sm rounded-tl-xl bg-primary/80 p-4 text-center md:px-3 md:py-2 lg:hidden" }, [
                createVNode("p", { class: "mb-0 text-main-bg-white" }, toDisplayString(__props.title), 1)
              ])
            ];
          }
        }),
        _: 1
      }, _parent));
      _push(`</div>`);
    };
  }
};
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("components/HoverImage.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const __nuxt_component_0 = _sfc_main;

export { __nuxt_component_0 as _ };
//# sourceMappingURL=HoverImage-812cf7ee.mjs.map
