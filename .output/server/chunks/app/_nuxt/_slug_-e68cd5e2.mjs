import { M as MetaHead, _ as __nuxt_component_0 } from './ListContainer-7adf6140.mjs';
import { _ as __nuxt_component_2$1 } from './nuxt-img-c5557f00.mjs';
import { withAsyncContext, computed, unref, withCtx, createVNode, useSSRContext, mergeProps } from 'vue';
import { ssrRenderAttrs, ssrRenderComponent, ssrRenderList, ssrInterpolate } from 'vue/server-renderer';
import { u as useAssets } from './useAssets-ef8ea7b3.mjs';
import { a as SNORKELING_ITEMS, b as SCUBA_DIVING_ITEMS, F as FREE_DIVING_ITEMS, S as SEO_DESCRIPTION } from './index-ab0f4448.mjs';
import { _ as _export_sfc } from './_plugin-vue_export-helper-cc2b3d55.mjs';
import { f as useRoute, g as useSeoMeta } from '../server.mjs';
import { u as useAsyncData, q as queryContent } from './query-003efbbe.mjs';
import { s as setSeoMeta } from './common-6de92c75.mjs';
import './nuxt-link-744fccd2.mjs';
import 'ufo';
import './navigation-18ec306d.mjs';
import 'ohash';
import './utils-c338605d.mjs';
import './ssr-cd1cbb71.mjs';
import './preview-c5948799.mjs';
import 'cookie-es';
import 'h3';
import 'destr';
import 'defu';
import '../../handlers/renderer.mjs';
import 'vue-bundle-renderer/runtime';
import 'devalue';
import '../../nitro/node-server.mjs';
import 'node-fetch-native/polyfill';
import 'node:http';
import 'node:https';
import 'ofetch';
import 'unenv/runtime/fetch/index';
import 'hookable';
import 'scule';
import 'klona';
import 'unstorage';
import 'unstorage/drivers/overlay';
import 'unstorage/drivers/memory';
import 'radix3';
import 'node:fs';
import 'node:url';
import 'pathe';
import 'unified';
import 'mdast-util-to-string';
import 'micromark/lib/preprocess.js';
import 'micromark/lib/postprocess.js';
import 'unist-util-stringify-position';
import 'micromark-util-character';
import 'micromark-util-chunked';
import 'micromark-util-resolve-all';
import 'remark-emoji';
import 'rehype-slug';
import 'remark-squeeze-paragraphs';
import 'rehype-external-links';
import 'remark-gfm';
import 'rehype-sort-attribute-values';
import 'rehype-sort-attributes';
import 'rehype-raw';
import 'remark-mdc';
import 'remark-parse';
import 'remark-rehype';
import 'mdast-util-to-hast';
import 'detab';
import 'unist-builder';
import 'mdurl';
import 'slugify';
import 'unist-util-position';
import 'unist-util-visit';
import 'shiki-es';
import 'unenv/runtime/npm/consola';
import 'ipx';
import 'http-graceful-shutdown';
import 'pathe/utils';
import 'unctx';
import 'vue-router';
import '@unhead/ssr';
import 'unhead';
import '@unhead/shared';
import './Icon-78f87aca.mjs';
import './config-6f8e3cb8.mjs';
import '@iconify/vue/dist/offline';
import '@iconify/vue';

const _sfc_main$1 = {
  __name: "AlbumEquipment",
  __ssrInlineRender: true,
  props: {
    data: {
      type: Array,
      required: true
    }
  },
  setup(__props) {
    const altDescription = (alt) => {
      return `${alt},${SEO_DESCRIPTION}`;
    };
    return (_ctx, _push, _parent, _attrs) => {
      const _component_nuxt_img = __nuxt_component_2$1;
      _push(`<div${ssrRenderAttrs(mergeProps({ class: "grid-album mt-6 min-h-[75vh] gap-4 overflow-y-auto pb-4 pr-2 md:grid md:h-[60vh] md:pr-4 md:pt-4" }, _attrs))} data-v-8ff1330f><!--[-->`);
      ssrRenderList(__props.data, (item, i) => {
        _push(`<div class="mb-3 mr-2 flex items-center overflow-hidden rounded-lg bg-[#F6F6F6] p-2 drop-shadow-[3px_3px_2px_rgba(0,0,0,0.25)] last:mb-0 md:mb-0 md:p-4" data-v-8ff1330f>`);
        _push(ssrRenderComponent(_component_nuxt_img, {
          format: "webp",
          quality: "80",
          src: unref(useAssets)(item.src),
          width: "120px",
          height: "120px",
          alt: altDescription(item.alt),
          class: "h-16 w-16 rounded bg-white object-contain object-center drop-shadow-[2px_2px_1px_rgba(0,0,0,0.25)] md:h-20 md:w-20",
          loading: "lazy"
        }, null, _parent));
        _push(`<div class="ml-4 flex-1" data-v-8ff1330f><p class="text-base font-bold md:text-lg" data-v-8ff1330f>${ssrInterpolate(item.title)}</p><p class="mt-1 text-base md:mt-2 md:text-lg" data-v-8ff1330f>${ssrInterpolate(item.price)}</p></div></div>`);
      });
      _push(`<!--]--></div>`);
    };
  }
};
const _sfc_setup$1 = _sfc_main$1.setup;
_sfc_main$1.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("components/AlbumEquipment.vue");
  return _sfc_setup$1 ? _sfc_setup$1(props, ctx) : void 0;
};
const __nuxt_component_2 = /* @__PURE__ */ _export_sfc(_sfc_main$1, [["__scopeId", "data-v-8ff1330f"]]);
const _sfc_main = {
  __name: "[slug]",
  __ssrInlineRender: true,
  async setup(__props) {
    var _a, _b;
    let __temp, __restore;
    const { path } = useRoute();
    const { data: page } = ([__temp, __restore] = withAsyncContext(() => useAsyncData(
      `content-${path}`,
      () => queryContent().where({ _path: path }).findOne()
    )), __temp = await __temp, __restore(), __temp);
    const title = ((_a = page.value) == null ? void 0 : _a.title) || "";
    const description = ((_b = page.value) == null ? void 0 : _b.description) || title;
    const getCurrentList = (pagePath) => {
      switch (pagePath) {
        case "/equipment/renting-freediving":
          return FREE_DIVING_ITEMS;
        case "/equipment/renting-scubadiving":
          return SCUBA_DIVING_ITEMS;
        default:
          return SNORKELING_ITEMS;
      }
    };
    const currentList = computed(() => {
      var _a2;
      return ((_a2 = page.value) == null ? void 0 : _a2._path) ? getCurrentList(page.value._path) : [];
    });
    useSeoMeta({ ...setSeoMeta(page) });
    return (_ctx, _push, _parent, _attrs) => {
      const _component_MetaHead = MetaHead;
      const _component_ListContainer = __nuxt_component_0;
      const _component_AlbumEquipment = __nuxt_component_2;
      _push(`<div${ssrRenderAttrs(_attrs)}>`);
      _push(ssrRenderComponent(_component_MetaHead, {
        title: unref(title),
        description: unref(description)
      }, null, _parent));
      _push(ssrRenderComponent(_component_ListContainer, { title: unref(title) }, {
        default: withCtx((_, _push2, _parent2, _scopeId) => {
          if (_push2) {
            _push2(ssrRenderComponent(_component_AlbumEquipment, { data: unref(currentList) }, null, _parent2, _scopeId));
          } else {
            return [
              createVNode(_component_AlbumEquipment, { data: unref(currentList) }, null, 8, ["data"])
            ];
          }
        }),
        _: 1
      }, _parent));
      _push(`</div>`);
    };
  }
};
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("pages/equipment/[slug].vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};

export { _sfc_main as default };
//# sourceMappingURL=_slug_-e68cd5e2.mjs.map
