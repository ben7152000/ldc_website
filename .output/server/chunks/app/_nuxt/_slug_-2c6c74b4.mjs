import { s as setSeoMeta, _ as __nuxt_component_0 } from './common-6de92c75.mjs';
import _sfc_main$1 from './ContentDoc-d4bdbd8b.mjs';
import { withAsyncContext, mergeProps, useSSRContext } from 'vue';
import { f as useRoute, g as useSeoMeta } from '../server.mjs';
import { u as useAsyncData, q as queryContent } from './query-003efbbe.mjs';
import { ssrRenderAttrs, ssrRenderComponent } from 'vue/server-renderer';
import { P as PAGE_PATH } from './index-ab0f4448.mjs';
import './nuxt-link-744fccd2.mjs';
import 'ufo';
import './Icon-78f87aca.mjs';
import './config-6f8e3cb8.mjs';
import 'klona';
import '@iconify/vue/dist/offline';
import '@iconify/vue';
import './_plugin-vue_export-helper-cc2b3d55.mjs';
import 'ofetch';
import 'hookable';
import 'unctx';
import 'vue-router';
import 'h3';
import '@unhead/ssr';
import 'unhead';
import '@unhead/shared';
import 'defu';
import '../../nitro/node-server.mjs';
import 'node-fetch-native/polyfill';
import 'node:http';
import 'node:https';
import 'destr';
import 'unenv/runtime/fetch/index';
import 'scule';
import 'ohash';
import 'unstorage';
import 'unstorage/drivers/overlay';
import 'unstorage/drivers/memory';
import 'radix3';
import 'node:fs';
import 'node:url';
import 'pathe';
import 'unified';
import 'mdast-util-to-string';
import 'micromark/lib/preprocess.js';
import 'micromark/lib/postprocess.js';
import 'unist-util-stringify-position';
import 'micromark-util-character';
import 'micromark-util-chunked';
import 'micromark-util-resolve-all';
import 'remark-emoji';
import 'rehype-slug';
import 'remark-squeeze-paragraphs';
import 'rehype-external-links';
import 'remark-gfm';
import 'rehype-sort-attribute-values';
import 'rehype-sort-attributes';
import 'rehype-raw';
import 'remark-mdc';
import 'remark-parse';
import 'remark-rehype';
import 'mdast-util-to-hast';
import 'detab';
import 'unist-builder';
import 'mdurl';
import 'slugify';
import 'unist-util-position';
import 'unist-util-visit';
import 'shiki-es';
import 'unenv/runtime/npm/consola';
import 'ipx';
import 'http-graceful-shutdown';
import './navigation-18ec306d.mjs';
import './utils-c338605d.mjs';
import './ssr-cd1cbb71.mjs';
import './preview-c5948799.mjs';
import 'cookie-es';
import './ContentRenderer-9978ff9b.mjs';
import './ContentRendererMarkdown-0d6708b2.mjs';
import 'property-information';
import './ContentQuery-9e1ec63f.mjs';

const _sfc_main = {
  __name: "[slug]",
  __ssrInlineRender: true,
  async setup(__props) {
    let __temp, __restore;
    const { path } = useRoute();
    const { data: page } = ([__temp, __restore] = withAsyncContext(() => useAsyncData(
      `content-${path}`,
      () => queryContent().where({ _path: path }).only(PAGE_PATH).findOne()
    )), __temp = await __temp, __restore(), __temp);
    useSeoMeta({ ...setSeoMeta(page) });
    return (_ctx, _push, _parent, _attrs) => {
      const _component_BreadcrumbNav = __nuxt_component_0;
      const _component_ContentDoc = _sfc_main$1;
      _push(`<div${ssrRenderAttrs(mergeProps({ class: "container mx-auto px-4 py-8 md:px-6 md:py-12" }, _attrs))}>`);
      _push(ssrRenderComponent(_component_BreadcrumbNav, null, null, _parent));
      _push(ssrRenderComponent(_component_ContentDoc, null, null, _parent));
      _push(`</div>`);
    };
  }
};
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("pages/diving/scubadiving/[slug].vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};

export { _sfc_main as default };
//# sourceMappingURL=_slug_-2c6c74b4.mjs.map
