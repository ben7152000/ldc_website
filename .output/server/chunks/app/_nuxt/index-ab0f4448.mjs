const PAGE_PATH = ["title", "description", "img"];
const CONTENT_QUERY = ["title", "description", "img", "alt", "_path"];
const SEO_DESCRIPTION = "\u6771\u5317\u89D2,\u5317\u6D77\u5CB8,\u9F8D\u6D1E,PADI,\u6F5B\u6C34\u4E2D\u5FC3,\u88DD\u5099\u51FA\u79DF\u4E2D\u5FC3,\u5834\u5730\u51FA\u79DF,\u8AB2\u7A0B\u5831\u540D,\u9AD4\u9A57\u5831\u540D,\u4E00\u65E5\u904A";
const SNORKELING_ITEMS = [
  {
    src: "mask.png",
    alt: "mask",
    title: "\u4E00\u822C\u86D9\u93E1\uFF08\u542B\u547C\u5438\u7BA1\uFF09",
    price: "NT$ 80"
  },
  {
    src: "mask.png",
    alt: "mask-degree",
    title: "\u6709\u5EA6\u6578\u86D9\u93E1\uFF08\u542B\u547C\u5438\u7BA1\uFF09",
    price: "NT$ 100"
  },
  {
    src: "snorkel.png",
    alt: "snorkel",
    title: "\u547C\u5438\u7BA1",
    price: "NT$ 40"
  },
  {
    src: "swim-goggles.png",
    alt: "swim-goggles",
    title: "\u6CF3\u93E1",
    price: "NT$ 60"
  },
  {
    src: "life-jacket.png",
    alt: "life-jacket",
    title: "\u6551\u751F\u8863",
    price: "NT$ 70"
  },
  {
    src: "kick-board.png",
    alt: "kick-board",
    title: "\u6D6E\u677F",
    price: "NT$ 70"
  },
  {
    src: "buoy.png",
    alt: "buoy",
    title: "\u6D6E\u6A19",
    price: "NT$ 70"
  },
  {
    src: "life-buoy.png",
    alt: "life-buoy",
    title: "\u6551\u751F\u5708",
    price: "NT$ 70"
  },
  {
    src: "water-shoes.png",
    alt: "water-shoes",
    title: "\u9632\u6ED1\u978B",
    price: "NT$ 60"
  },
  {
    src: "parasol.png",
    alt: "parasol",
    title: "\u967D\u5098",
    price: "NT$ 300"
  },
  {
    src: "open-heeled-fins.png",
    alt: "open-heeled-fins",
    title: "\u5957\u978B\u5F0F\u86D9\u978B",
    price: "NT$ 100"
  },
  {
    src: "closed-heel-fins.png",
    alt: "closed-heel-fins",
    title: "\u5957\u8173\u5F0F\u86D9\u978B",
    price: "NT$ 100"
  },
  {
    src: "diving-suit.png",
    alt: "diving-suit",
    title: "\u9023\u8EAB\u9632\u5BD2\u8863",
    price: "NT$ 160"
  },
  {
    src: "wetsuit-top.png",
    alt: "wetsuit-top",
    title: "\u9632\u5BD2\u8863",
    price: "NT$ 130"
  },
  {
    src: "wetsuit-bottom.png",
    alt: "wetsuit-bottom",
    title: "\u9632\u5BD2\u8932",
    price: "NT$ 120"
  },
  {
    src: "rashguard-top.png",
    alt: "rashguard-top",
    title: "\u6C34\u6BCD\u8863",
    price: "NT$ 100"
  },
  {
    src: "rashguard-bottom.png",
    alt: "rashguard-bottom",
    title: "\u6C34\u6BCD\u8932",
    price: "NT$ 100"
  },
  {
    src: "rashguard.png",
    alt: "rashguard",
    title: "\u6C34\u6BCD\u4E0A\u8863 + \u6C34\u6BCD\u8932",
    price: "NT$ 160"
  },
  {
    src: "rashguard-and-wetsuit.png",
    alt: "rashguard-and-wetsuit",
    title: "\u6C34\u6BCD\u4E0A\u8863 + \u9632\u5BD2\u8932",
    price: "NT$ 200"
  }
];
const SCUBA_DIVING_ITEMS = [
  {
    src: "bcd.jpg",
    alt: "bcd",
    title: "BCD",
    price: "NT$ 300"
  },
  {
    src: "regulator.jpg",
    alt: "regulator",
    title: "\u8ABF\u7BC0\u5668",
    price: "NT$ 300"
  },
  {
    src: "diving-suit.png",
    alt: "diving-suit",
    title: "\u9632\u5BD2\u8863",
    price: "NT$ 160"
  },
  {
    src: "open-heeled-fins.png",
    alt: "open-heeled-fins",
    title: "\u5957\u978B\u5F0F\u86D9\u978B",
    price: "NT$ 100"
  },
  {
    src: "closed-heel-fins.png",
    alt: "closed-heel-fins",
    title: "\u5957\u8173\u5F0F\u86D9\u978B",
    price: "NT$ 100"
  },
  {
    src: "water-shoes.png",
    alt: "water-shoes",
    title: "\u9632\u6ED1\u978B",
    price: "NT$ 60"
  }
];
const FREE_DIVING_ITEMS = [
  {
    src: "snorkel.png",
    alt: "snorkel",
    title: "\u547C\u5438\u7BA1",
    price: "NT$ 40"
  },
  {
    src: "freediving-mask.png",
    alt: "mask",
    title: "\u81EA\u6F5B\u86D9\u93E1\uFF08\u542B\u547C\u5438\u7BA1\uFF09",
    price: "NT$ 100"
  },
  {
    src: "long-fins.png",
    alt: "long-fins",
    title: "\u9577\u86D9\u978B",
    price: "NT$ 200"
  },
  {
    src: "lanyard.png",
    alt: "lanyard",
    title: "\u5B89\u5168\u7E6B\u9396",
    price: "NT$ 100"
  },
  {
    src: "freediving-buoy.png",
    alt: "buoy",
    title: "\u6D6E\u7403\u4E00\u7D44",
    price: "NT$ 300"
  },
  {
    src: "lope.png",
    alt: "lope",
    title: "\u7E69\u5B50",
    price: "NT$ 80"
  },
  {
    src: "bottom-weight.png",
    alt: "bottom-weight",
    title: "\u5E95\u925B",
    price: "NT$ 80"
  },
  {
    src: "weight.png",
    alt: "weight",
    title: "\u925B\u584A\uFF081 kg\uFF09",
    price: "NT$ 20"
  },
  {
    src: "weight-belt.png",
    alt: "weight-belt",
    title: "\u925B\u5E36",
    price: "NT$ 70"
  },
  {
    src: "freediving-suit.png",
    alt: "diving-suit",
    title: "\u81EA\u6F5B\u9632\u5BD2\u8863",
    price: "NT$ 300"
  }
];

export { CONTENT_QUERY as C, FREE_DIVING_ITEMS as F, PAGE_PATH as P, SEO_DESCRIPTION as S, SNORKELING_ITEMS as a, SCUBA_DIVING_ITEMS as b };
//# sourceMappingURL=index-ab0f4448.mjs.map
