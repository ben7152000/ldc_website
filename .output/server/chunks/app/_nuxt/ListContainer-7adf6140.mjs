import { useSSRContext, ref, computed, withCtx, unref, createTextVNode, toDisplayString, createVNode, defineComponent, mergeProps } from 'vue';
import { f as useRoute, u as useHead } from '../server.mjs';
import { ssrRenderComponent, ssrInterpolate, ssrRenderAttrs, ssrRenderSlot, ssrRenderList, ssrRenderClass } from 'vue/server-renderer';
import { S as SEO_DESCRIPTION } from './index-ab0f4448.mjs';
import { _ as __nuxt_component_0$3 } from './nuxt-link-744fccd2.mjs';
import { _ as __nuxt_component_0$2 } from './common-6de92c75.mjs';
import { _ as __nuxt_component_2 } from './nuxt-img-c5557f00.mjs';
import { u as useAsyncData } from './query-003efbbe.mjs';
import { f as fetchContentNavigation } from './navigation-18ec306d.mjs';
import { _ as _export_sfc } from './_plugin-vue_export-helper-cc2b3d55.mjs';

const removeUndefinedProps = (props) => Object.fromEntries(Object.entries(props).filter(([, value]) => value !== void 0));
const setupForUseMeta = (metaFactory, renderChild) => (props, ctx) => {
  useHead(() => metaFactory({ ...removeUndefinedProps(props), ...ctx.attrs }, ctx));
  return () => {
    var _a, _b;
    return renderChild ? (_b = (_a = ctx.slots).default) == null ? void 0 : _b.call(_a) : null;
  };
};
const globalProps = {
  accesskey: String,
  autocapitalize: String,
  autofocus: {
    type: Boolean,
    default: void 0
  },
  class: [String, Object, Array],
  contenteditable: {
    type: Boolean,
    default: void 0
  },
  contextmenu: String,
  dir: String,
  draggable: {
    type: Boolean,
    default: void 0
  },
  enterkeyhint: String,
  exportparts: String,
  hidden: {
    type: Boolean,
    default: void 0
  },
  id: String,
  inputmode: String,
  is: String,
  itemid: String,
  itemprop: String,
  itemref: String,
  itemscope: String,
  itemtype: String,
  lang: String,
  nonce: String,
  part: String,
  slot: String,
  spellcheck: {
    type: Boolean,
    default: void 0
  },
  style: String,
  tabindex: String,
  title: String,
  translate: String
};
const Title = /* @__PURE__ */ defineComponent({
  // eslint-disable-next-line vue/no-reserved-component-names
  name: "Title",
  inheritAttrs: false,
  setup: setupForUseMeta((_, { slots }) => {
    var _a, _b, _c;
    return {
      title: ((_c = (_b = (_a = slots.default) == null ? void 0 : _a.call(slots)) == null ? void 0 : _b[0]) == null ? void 0 : _c.children) || null
    };
  })
});
const Meta = /* @__PURE__ */ defineComponent({
  // eslint-disable-next-line vue/no-reserved-component-names
  name: "Meta",
  inheritAttrs: false,
  props: {
    ...globalProps,
    charset: String,
    content: String,
    httpEquiv: String,
    name: String,
    body: Boolean,
    renderPriority: [String, Number]
  },
  setup: setupForUseMeta((props) => {
    const meta = { ...props };
    if (meta.httpEquiv) {
      meta["http-equiv"] = meta.httpEquiv;
      delete meta.httpEquiv;
    }
    return {
      meta: [meta]
    };
  })
});
const Head = /* @__PURE__ */ defineComponent({
  // eslint-disable-next-line vue/no-reserved-component-names
  name: "Head",
  inheritAttrs: false,
  setup: (_props, ctx) => () => {
    var _a, _b;
    return (_b = (_a = ctx.slots).default) == null ? void 0 : _b.call(_a);
  }
});
const _sfc_main$2 = {
  __name: "MetaHead",
  __ssrInlineRender: true,
  props: {
    title: {
      type: String,
      required: true
    },
    description: {
      type: String,
      required: true
    }
  },
  setup(__props) {
    const props = __props;
    const titleDescription = ref(
      "\u3010\u9F8D\u6D1E\u5CAC\u3011\u5317\u90E8\u6C34\u4E0A\u6D3B\u52D5 - \u6D6E\u6F5B\u3001\u6C34\u80BA\u6F5B\u6C34\u3001SUP\u3001\u6500\u5CA9\u3001\u81EA\u7531\u6F5B\u6C34\u88DD\u5099\u79DF\u501F"
    );
    const headTitle = computed(() => `${titleDescription.value} - ${props.title}`);
    const headDescription = computed(() => `${props.description},${SEO_DESCRIPTION}`);
    return (_ctx, _push, _parent, _attrs) => {
      const _component_Head = Head;
      const _component_Title = Title;
      const _component_Meta = Meta;
      _push(ssrRenderComponent(_component_Head, _attrs, {
        default: withCtx((_, _push2, _parent2, _scopeId) => {
          if (_push2) {
            _push2(ssrRenderComponent(_component_Title, null, {
              default: withCtx((_2, _push3, _parent3, _scopeId2) => {
                if (_push3) {
                  _push3(`${ssrInterpolate(unref(headTitle))}`);
                } else {
                  return [
                    createTextVNode(toDisplayString(unref(headTitle)), 1)
                  ];
                }
              }),
              _: 1
            }, _parent2, _scopeId));
            _push2(ssrRenderComponent(_component_Meta, {
              name: "description",
              content: unref(headDescription)
            }, null, _parent2, _scopeId));
          } else {
            return [
              createVNode(_component_Title, null, {
                default: withCtx(() => [
                  createTextVNode(toDisplayString(unref(headTitle)), 1)
                ]),
                _: 1
              }),
              createVNode(_component_Meta, {
                name: "description",
                content: unref(headDescription)
              }, null, 8, ["content"])
            ];
          }
        }),
        _: 1
      }, _parent));
    };
  }
};
const _sfc_setup$2 = _sfc_main$2.setup;
_sfc_main$2.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("components/MetaHead.vue");
  return _sfc_setup$2 ? _sfc_setup$2(props, ctx) : void 0;
};
const MetaHead = _sfc_main$2;
const _sfc_main$1 = {
  __name: "SubMenuSide",
  __ssrInlineRender: true,
  props: {
    subMenu: {
      type: Array,
      default: () => []
    },
    path: {
      type: String,
      required: true
    }
  },
  setup(__props) {
    return (_ctx, _push, _parent, _attrs) => {
      const _component_NuxtLink = __nuxt_component_0$3;
      _push(`<div${ssrRenderAttrs(mergeProps({ class: "hidden pl-12 pr-8 pt-16 md:block" }, _attrs))}><ul><!--[-->`);
      ssrRenderList(__props.subMenu, (sub, i) => {
        _push(`<li class="${ssrRenderClass([{ "text-primary": sub._path === __props.path }, "px-4 py-2 transition-colors duration-300 hover:text-primary"])}">`);
        _push(ssrRenderComponent(_component_NuxtLink, {
          to: sub._path
        }, {
          default: withCtx((_, _push2, _parent2, _scopeId) => {
            if (_push2) {
              _push2(`${ssrInterpolate(sub.title)}`);
            } else {
              return [
                createTextVNode(toDisplayString(sub.title), 1)
              ];
            }
          }),
          _: 2
        }, _parent));
        _push(`</li>`);
      });
      _push(`<!--]--></ul></div>`);
    };
  }
};
const _sfc_setup$1 = _sfc_main$1.setup;
_sfc_main$1.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("components/SubMenuSide.vue");
  return _sfc_setup$1 ? _sfc_setup$1(props, ctx) : void 0;
};
const __nuxt_component_0$1 = _sfc_main$1;
const _sfc_main = {
  __name: "ListContainer",
  __ssrInlineRender: true,
  props: {
    title: {
      type: String,
      required: true
    }
  },
  setup(__props) {
    var _a;
    const { path } = useRoute();
    const { data: nav } = useAsyncData("navigation", () => fetchContentNavigation());
    const subMenu = (_a = nav.value) == null ? void 0 : _a.reduce((prev, curr) => {
      if (!curr.children) {
        return [...prev, { title: curr.title, _path: curr._path }];
      }
      const child = curr.children.filter((o) => o.title !== curr.title).map((o) => ({ title: o.title, _path: o._path }));
      return [...prev, ...child];
    }, []);
    return (_ctx, _push, _parent, _attrs) => {
      const _component_SubMenuSide = __nuxt_component_0$1;
      const _component_BreadcrumbNav = __nuxt_component_0$2;
      const _component_nuxt_img = __nuxt_component_2;
      _push(`<div${ssrRenderAttrs(_attrs)} data-v-eb58208a><div class="relative mx-auto -mt-2 py-0 md:container md:mt-0 md:px-6 md:py-12" data-v-eb58208a><div class="relative min-h-[80vh] max-w-full rounded-br-30 border-b-[24px] border-b-secondary bg-main-bg-gray md:rounded-tl-30" data-v-eb58208a><span class="absolute left-0 top-0 z-10 hidden h-[5%] w-[180px] rounded-tl-30 bg-primary md:inline-block" data-v-eb58208a></span><div class="flex" data-v-eb58208a>`);
      _push(ssrRenderComponent(_component_SubMenuSide, {
        "sub-menu": unref(subMenu),
        path: unref(path)
      }, null, _parent));
      _push(`<div class="flex-1 px-2 py-8 md:px-4 md:py-10 md:pb-16" data-v-eb58208a>`);
      _push(ssrRenderComponent(_component_BreadcrumbNav, null, null, _parent));
      _push(`<h1 class="mt-10 text-2xl" data-v-eb58208a><span class="relative" data-v-eb58208a><span class="absolute -bottom-1 left-0 h-12 w-12 rounded-tr-30 bg-highlight" data-v-eb58208a></span><span class="animate__animated animate__fadeIn relative pl-4" data-v-eb58208a>${ssrInterpolate(__props.title)}</span></span></h1>`);
      ssrRenderSlot(_ctx.$slots, "default", {}, null, _push, _parent);
      _push(`</div></div></div></div>`);
      _push(ssrRenderComponent(_component_nuxt_img, {
        format: "webp",
        quality: "80",
        src: "/images/subtract-front-2.svg",
        alt: unref(SEO_DESCRIPTION),
        class: "absolute right-0 top-[100px] hidden opacity-80 lg:inline-block"
      }, null, _parent));
      _push(ssrRenderComponent(_component_nuxt_img, {
        format: "webp",
        quality: "80",
        src: "/images/subtract-back-2.svg",
        alt: unref(SEO_DESCRIPTION),
        class: "absolute bottom-[72px] left-0 hidden opacity-80 lg:inline-block"
      }, null, _parent));
      _push(`</div>`);
    };
  }
};
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("components/ListContainer.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const __nuxt_component_0 = /* @__PURE__ */ _export_sfc(_sfc_main, [["__scopeId", "data-v-eb58208a"]]);

export { MetaHead as M, __nuxt_component_0 as _ };
//# sourceMappingURL=ListContainer-7adf6140.mjs.map
