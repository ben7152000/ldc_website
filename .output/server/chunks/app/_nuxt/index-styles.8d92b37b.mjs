import { b as buildAssetsURL } from '../../handlers/renderer.mjs';
import 'vue-bundle-renderer/runtime';
import 'h3';
import 'devalue';
import 'vue/server-renderer';
import '../../nitro/node-server.mjs';
import 'node-fetch-native/polyfill';
import 'node:http';
import 'node:https';
import 'destr';
import 'ofetch';
import 'unenv/runtime/fetch/index';
import 'hookable';
import 'scule';
import 'klona';
import 'defu';
import 'ohash';
import 'ufo';
import 'unstorage';
import 'unstorage/drivers/overlay';
import 'unstorage/drivers/memory';
import 'radix3';
import 'node:fs';
import 'node:url';
import 'pathe';
import 'unified';
import 'mdast-util-to-string';
import 'micromark/lib/preprocess.js';
import 'micromark/lib/postprocess.js';
import 'unist-util-stringify-position';
import 'micromark-util-character';
import 'micromark-util-chunked';
import 'micromark-util-resolve-all';
import 'remark-emoji';
import 'rehype-slug';
import 'remark-squeeze-paragraphs';
import 'rehype-external-links';
import 'remark-gfm';
import 'rehype-sort-attribute-values';
import 'rehype-sort-attributes';
import 'rehype-raw';
import 'remark-mdc';
import 'remark-parse';
import 'remark-rehype';
import 'mdast-util-to-hast';
import 'detab';
import 'unist-builder';
import 'mdurl';
import 'slugify';
import 'unist-util-position';
import 'unist-util-visit';
import 'shiki-es';
import 'unenv/runtime/npm/consola';
import 'ipx';
import 'http-graceful-shutdown';

const IndexHero_vue_vue_type_style_index_0_scoped_f924e788_lang = "@media (min-width:768px){.hero-bg[data-v-f924e788]{background-image:url(" + buildAssetsURL("hero-bg.c591c5f5.png") + ");background-position:top;background-repeat:no-repeat;background-size:cover;border-top-right-radius:80px}}.triangle[data-v-f924e788]{border-color:transparent transparent #34b1b1;border-style:solid;border-width:0 15px 15px;color:#abaaaa;height:0;left:50%;top:100%;transform:rotate(180deg) translateX(50%);width:0}";

const IndexFeature_vue_vue_type_style_index_0_lang = ".features{opacity:1;transition:all .3s ease-in-out}.features.show{opacity:1}.features .feature-card{opacity:1;transition:all .6s ease-in-out}.features.show .feature-card{opacity:1}.features.show .feature-card:first-child{transform:translateY(0);transition-delay:.3s}.features.show .feature-card:nth-child(2){transform:translateY(0);transition-delay:.6s}.features.show .feature-card:nth-child(3){transform:translateY(0);transition-delay:.9s}";

const IndexActivities_vue_vue_type_style_index_0_scoped_ddad5949_lang = "[data-v-ddad5949] .swiper{border-radius:12px!important;padding-bottom:40px!important}";

const index_vue_vue_type_style_index_0_scoped_13dcf4e7_lang = "section[data-v-13dcf4e7]{opacity:0;transition:all .5s ease-in-out}.show[data-v-13dcf4e7]{opacity:1;transform:translateX(0)}section#proposal[data-v-13dcf4e7]{opacity:0;transform:translateX(-5%);transition-delay:2s;transition:all .5s ease-in-out}section#proposal.show[data-v-13dcf4e7]{opacity:1;transform:translateX(0)}@media (min-width:768px){section#proposal[data-v-13dcf4e7]{opacity:0;transform:translateY(-10%);transition-delay:2s;transition:all 1s ease-in-out}section#proposal.show[data-v-13dcf4e7]{opacity:1;transform:translateY(0)}}";

const indexStyles_8d92b37b = [IndexHero_vue_vue_type_style_index_0_scoped_f924e788_lang, IndexFeature_vue_vue_type_style_index_0_lang, IndexActivities_vue_vue_type_style_index_0_scoped_ddad5949_lang, index_vue_vue_type_style_index_0_scoped_13dcf4e7_lang, index_vue_vue_type_style_index_0_scoped_13dcf4e7_lang];

export { indexStyles_8d92b37b as default };
//# sourceMappingURL=index-styles.8d92b37b.mjs.map
