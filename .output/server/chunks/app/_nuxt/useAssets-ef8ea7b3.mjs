import { b as buildAssetsURL } from '../../handlers/renderer.mjs';
import { filename } from 'pathe/utils';

const abstractWaves$1 = "" + buildAssetsURL("abstract-waves.223d89b0.svg");
const __vite_glob_0_0 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: abstractWaves$1
});
const lineIcon = "" + buildAssetsURL("line-icon.6b53468a.svg");
const __vite_glob_0_1 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: lineIcon
});
const logoDark = "" + buildAssetsURL("logo-dark.bff68b22.svg");
const __vite_glob_0_2 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: logoDark
});
const longdongcape = "" + buildAssetsURL("longdongcape.c5755f30.svg");
const __vite_glob_0_3 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: longdongcape
});
const substractBackXs = "" + buildAssetsURL("substract-back-xs.5a27df8b.svg");
const __vite_glob_0_4 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: substractBackXs
});
const substractFrontXs = "" + buildAssetsURL("substract-front-xs.39282275.svg");
const __vite_glob_0_5 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: substractFrontXs
});
const subtractBack2 = "" + buildAssetsURL("subtract-back-2.2ae921b6.svg");
const __vite_glob_0_6 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: subtractBack2
});
const subtractFront2 = "" + buildAssetsURL("subtract-front-2.e650943f.svg");
const __vite_glob_0_7 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: subtractFront2
});
const abstractWaves = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHwAAABKCAYAAAB5PTAOAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAhYSURBVHgB7V1dcts2EF5Qfuhb3BOEOYEVu53pm+UTxD6BnF4gSS8QJRdIfILaJ4hzAslJ3zqtlROYOUGUt04cY7sLkBQpUSRAghIV45tR5NAE/z5gsdhvlwbw8PDw8PDw2DRw3N/F8W+hTZsAPLYS+NfBEILgBoJvN3i1f0PkH5u0E+CxdcCP+y9Bwmhh8wykfCSOprOytjvgsTVgEw69gMl+XvDrXfr06TMpO4YnfEtAZIdkwt8BKlJXIYIK+Dl8C4Djg2Mi+xqghGwJr8icR1ABP4d3GKkJx0ITPoci+98RGMAT3lHg+JcBBPJP+jEs2YscNfGCyD4HQ3jCOwY9V/eIaBxU7BmBxBMy41OwgCe8I1Dmeyd4VrDcKtgZLgHl06olWBE84RtGhmiep3cr9mYTzvP1W6gJT/iGoEz3TjA0I5ob4IQ+T0088TJ4wtcM7Yzhy+o5Om3ReFRn0XnClcmDnwpGwH+zOnPYJhB73BzrHoLJaE4bwjnN1S9c3mcnCNeKzy0FFbAPPfGQTBcHGPjBhAbNZ/ojIrqbGbX9TD/f0Kj4RN/UKf628mJdQHfSHo/kAdiSrA6gzDcHUibgGBshXBEc3D6hs/fpxrjn2z0Qe0xBUIe4Q+4E1AGCyGVH0B32GxGsImGHAKXhz5IDtUd0grURnjFrT8Bs5K4DuiMgCQ4rLMJcb76jTinpZ9SWZ26J+PfNOqwDohfW71NSzp4WrdFbJbz23HUvoJyxM+pEb5vO0fFzfgf5Z1wolzpXy9T8FfSYYCJaDsAjAyIZafQ5NNv48YDW8LLIgyfyAx5s59mNzgiP5TteUw5js9fgYBQ2VHMtO2HwFbRjtgwBD0HdWGxmhQihc2CSBUfGrug/l6487rk2jiXCiowWtzQ26cqc9PBZ7HzVOIAidwJC0rwD/FCiJg9FdTwmP4A9kMEjELgH3CmEqOdI2V9BPIoF3YuctOGApdp4qXOIU3F4/Xhxa23C9UnFiA4xtGypH4jASyL4fdPIkdWZx/0BaFM3UB2BVwkg6lujxBIh0FJQskWatH0/2oTjCMp8IokXtOfzooFjTbhVkH/eKp276D/TLgVM9JqZiE8tAjzI78BkMlLzyN/ROjuqugxTFa1CG7ci3Kh35RrQckOP5IttiYp1DebiilkI1ohwc402adB+AOE+AMf7p2R5KO5eFbcw18YrCbca1Tr2+2rd5u5Hg5XAIoHX8iNTCypWn9RiVHuinUCPaHaCTSwpm3All16CBUTxiQsjNwU7etPdFHYJEDEsR3UWS4SvqGpY3KtW7/LQ0CsDioIZj+akYfMBJnIX0QvekHk+LW3RoHfZQgsX30MtWqhgCgdQkmXTLl2rHhEikUgJdxgvo3j5FEQsiJD0GokNrxIWSOaAicX6nwYYr6sPpxfQECK+GJPITRSP6gm0ABz/SmtheRhLpm5UqDxmKkjCnaMlmTQLN5KpO4ElgSb8w8G70tCoNiUnLkdJ2uN74nBNmvgq6I4A1AkkhXjhbmrqfM6zcW45cENxfaTAjZJNB9DofnhwiYumRMfePgWU4LM4+kdNv5rwq31c2cqiqqH6ArJKmsXctRlEFb8PwTUcOsHLvhiSVoFHiVrGvWihRyrH7MTJyXO6OG5qJNsihLUgNduXtkUFhUdLfDG56IuxeCSONeFSvqI5/E2mGc/XR03X1fMAQgu6uBIuMuikNLoCfO0o3sckT8ARDCpMw6yXPtBepLxpGvs2DwmuOsCSCsXXor6rrisWQ7Q+zt88t25EJs1dVVYhvHIxkpfOYKSiySOnKU5mBXCFLWexk9K6xJiqY+oag74TmTR/BiY30isCrfG3QXB6NvPltPLFnBBun1wPiVm7aCtJwBb04LgT9HUnYIkUw9V7C73uT7NxlHSaWKAI1oQ6FaaNCDeuX86f/ML13HXfoOZqPaqPK/Zc8sXqZ7xYaePuAwj3EVZxd4nn1GKpaqVOxouFNs7OF3UKncrkiW4AC228NBHCLuOF3w12h3ygXYOTnrkK2NxnWK14EMf0+b3MjzDNeDGcq73pdoGM6T4FoxUPfqHn/tqkwrQ644W9Vy2shOU7uqlfvs+oVamDQIEW+Yfpcy8l3Mwxc1u/bAKtRHGt110SRAkLdwwE9Xz82iWZdBHz0SwGlstaNt+vbVc7q1OcPuy/qTThLY9qHSTZ6SvFx51syoRH+WpS7gzf15Y+rUbyDknBtiTr1hE7wnW18eIUp0qy2xvVzR5GY2jyJV650svT2ndVKy727JMf0iMpybSpI1xM+NX+l9UXpWS2E1ejOh3FPRxuWBcvQ1xWjJ/JkZrNp4oMAvGAtv2syohVNk5KbLP70bn95y6yXRirigkL5FKI05vQSXpT3kGRnK7UZSTTSfyyUix4aWm8DV3ciCpApOlSnrmOSBYTLuVZXi7lbfCiqQnPLDeIaNkHjzxylTrXrfgTZXnpp2TGhurdKbqn1Z7LaqXiFh4oI5tqGZcTFmkbrHo4u+mnEzJpAeYkr6Ww0qk8uohmRKdlt59YUQMtmzrt9bFCFsYK2Z5SyNruCIhpKTG0cE9VaIXw+kTP1TTYYJVp846gOutsIYmDPxuPAzgnXL/bG3n+Dw1bbI1kmsmmKfK+I/6n65FGZ4TXqjD1pcRrh5uMF6sKU1+Ptkk0zXixqDD1RHcBDd7x4itMtxG1CDevMF2viuZRDbuMF9OU2BbeAuzhBsaEG1aY+rrxjsPipT7BGMrW1j7jZStg9upN9QK+ErJVVcP1CDw6D8N3rQYPoVC/9CZ822D2pyglpwItQlU1PPZkbxcM//aoHOlcqgS6uNzP1z84uKQ4VpI8PDw8PDw8PDzWiP8BO5tH0XK93ogAAAAASUVORK5CYII=";
const __vite_glob_1_0 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: abstractWaves
});
const bottomWeight = "" + buildAssetsURL("bottom-weight.4260eca5.png");
const __vite_glob_1_1 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: bottomWeight
});
const fins = "" + buildAssetsURL("fins.e57e39c6.png");
const __vite_glob_1_2 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: fins
});
const freedivingBuoy = "" + buildAssetsURL("freediving-buoy.9102fcb6.png");
const __vite_glob_1_3 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: freedivingBuoy
});
const freedivingMask = "" + buildAssetsURL("freediving-mask.171f65d2.png");
const __vite_glob_1_4 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: freedivingMask
});
const freedivingSuit = "" + buildAssetsURL("freediving-suit.c9e845c7.png");
const __vite_glob_1_5 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: freedivingSuit
});
const lanyard = "" + buildAssetsURL("lanyard.e80e4db3.png");
const __vite_glob_1_6 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: lanyard
});
const longFins = "" + buildAssetsURL("long-fins.386d62fd.png");
const __vite_glob_1_7 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: longFins
});
const lope = "" + buildAssetsURL("lope.0389c95b.png");
const __vite_glob_1_8 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: lope
});
const snorkel$1 = "" + buildAssetsURL("snorkel.14b8202e.png");
const __vite_glob_1_9 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: snorkel$1
});
const weightBelt = "" + buildAssetsURL("weight-belt.c6261a0f.png");
const __vite_glob_1_10 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: weightBelt
});
const weight = "" + buildAssetsURL("weight.a932a22b.png");
const __vite_glob_1_11 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: weight
});
const closedHeelFins$1 = "" + buildAssetsURL("closed-heel-fins.432cce56.png");
const __vite_glob_1_12 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: closedHeelFins$1
});
const divingSuit$1 = "" + buildAssetsURL("diving-suit.88d581e7.png");
const __vite_glob_1_13 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: divingSuit$1
});
const openHeeledFins$1 = "" + buildAssetsURL("open-heeled-fins.0e606c78.png");
const __vite_glob_1_14 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: openHeeledFins$1
});
const buoy = "" + buildAssetsURL("buoy.40ebbb5c.png");
const __vite_glob_1_15 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: buoy
});
const closedHeelFins = "" + buildAssetsURL("closed-heel-fins.432cce56.png");
const __vite_glob_1_16 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: closedHeelFins
});
const divingSuit = "" + buildAssetsURL("diving-suit.47cdd026.png");
const __vite_glob_1_17 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: divingSuit
});
const kickBoard = "" + buildAssetsURL("kick-board.7345306d.png");
const __vite_glob_1_18 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: kickBoard
});
const lifeBuoy = "" + buildAssetsURL("life-buoy.0867957a.png");
const __vite_glob_1_19 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: lifeBuoy
});
const lifeJacket = "" + buildAssetsURL("life-jacket.930620bb.png");
const __vite_glob_1_20 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: lifeJacket
});
const mask = "" + buildAssetsURL("mask.2692be4f.png");
const __vite_glob_1_21 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: mask
});
const openHeeledFins = "" + buildAssetsURL("open-heeled-fins.0e606c78.png");
const __vite_glob_1_22 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: openHeeledFins
});
const parasol = "" + buildAssetsURL("parasol.c307b6a7.png");
const __vite_glob_1_23 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: parasol
});
const rashguardAndWetsuit = "" + buildAssetsURL("rashguard-and-wetsuit.0c66bbfe.png");
const __vite_glob_1_24 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: rashguardAndWetsuit
});
const rashguardBottom = "" + buildAssetsURL("rashguard-bottom.32da7ed5.png");
const __vite_glob_1_25 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: rashguardBottom
});
const rashguardTop = "" + buildAssetsURL("rashguard-top.422b6a3c.png");
const __vite_glob_1_26 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: rashguardTop
});
const rashguard = "" + buildAssetsURL("rashguard.95f26b58.png");
const __vite_glob_1_27 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: rashguard
});
const snorkel = "" + buildAssetsURL("snorkel.14b8202e.png");
const __vite_glob_1_28 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: snorkel
});
const swimGoggles = "" + buildAssetsURL("swim-goggles.a4affe7c.png");
const __vite_glob_1_29 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: swimGoggles
});
const waterShoes = "" + buildAssetsURL("water-shoes.78aaf795.png");
const __vite_glob_1_30 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: waterShoes
});
const wetsuitBottom = "" + buildAssetsURL("wetsuit-bottom.45eb304f.png");
const __vite_glob_1_31 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: wetsuitBottom
});
const wetsuitTop = "" + buildAssetsURL("wetsuit-top.befa17de.png");
const __vite_glob_1_32 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: wetsuitTop
});
const heroBg = "" + buildAssetsURL("hero-bg.c591c5f5.png");
const __vite_glob_1_33 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: heroBg
});
const logoGray = "" + buildAssetsURL("logo-gray.edb8267d.png");
const __vite_glob_1_34 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: logoGray
});
const logoNew = "" + buildAssetsURL("logo-new.ea77e4fc.png");
const __vite_glob_1_35 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: logoNew
});
const logo = "" + buildAssetsURL("logo.e2818a24.png");
const __vite_glob_1_36 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: logo
});
const about1 = "" + buildAssetsURL("about-1.eff680da.jpg");
const __vite_glob_2_0 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: about1
});
const aow = "" + buildAssetsURL("aow.3b7f8a13.jpg");
const __vite_glob_2_1 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: aow
});
const fundive2 = "" + buildAssetsURL("fundive-2.05ffd575.jpg");
const __vite_glob_2_2 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: fundive2
});
const fundive = "" + buildAssetsURL("fundive.f7407d6c.jpg");
const __vite_glob_2_3 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: fundive
});
const longdongView1 = "" + buildAssetsURL("longdong-view-1.74ee922c.jpg");
const __vite_glob_2_4 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: longdongView1
});
const longdongView2 = "" + buildAssetsURL("longdong-view-2.7b6e7af0.jpg");
const __vite_glob_2_5 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: longdongView2
});
const longdongView3 = "" + buildAssetsURL("longdong-view-3.d8311392.jpg");
const __vite_glob_2_6 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: longdongView3
});
const ow = "" + buildAssetsURL("ow.e804b873.jpg");
const __vite_glob_2_7 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: ow
});
const rockClimbing = "" + buildAssetsURL("rock-climbing.b421732c.jpg");
const __vite_glob_2_8 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: rockClimbing
});
const scubaExperience = "" + buildAssetsURL("scuba-experience.843df2d7.jpg");
const __vite_glob_2_9 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: scubaExperience
});
const snorkeling2 = "" + buildAssetsURL("snorkeling-2.ef0d5a3d.jpg");
const __vite_glob_2_10 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: snorkeling2
});
const snorkeling = "" + buildAssetsURL("snorkeling.7d92641b.jpg");
const __vite_glob_2_11 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: snorkeling
});
const sup2 = "" + buildAssetsURL("sup-2.dedc2e5a.jpg");
const __vite_glob_2_12 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: sup2
});
const sup = "" + buildAssetsURL("sup.6a5c5839.jpg");
const __vite_glob_2_13 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: sup
});
const bcd = "" + buildAssetsURL("bcd.63a3861f.jpg");
const __vite_glob_2_14 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: bcd
});
const regulator = "" + buildAssetsURL("regulator.70e2495c.jpg");
const __vite_glob_2_15 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: regulator
});
const proposal2 = "" + buildAssetsURL("proposal-2.291c4d1f.jpg");
const __vite_glob_2_16 = /* @__PURE__ */ Object.freeze({
  __proto__: null,
  default: proposal2
});
function useAssets(path) {
  let assets;
  if (/(\.svg)$/i.exec(path)) {
    assets = /* @__PURE__ */ Object.assign({ "/public/images/abstract-waves.svg": __vite_glob_0_0, "/public/images/line-icon.svg": __vite_glob_0_1, "/public/images/logo-dark.svg": __vite_glob_0_2, "/public/images/longdongcape.svg": __vite_glob_0_3, "/public/images/substract-back-xs.svg": __vite_glob_0_4, "/public/images/substract-front-xs.svg": __vite_glob_0_5, "/public/images/subtract-back-2.svg": __vite_glob_0_6, "/public/images/subtract-front-2.svg": __vite_glob_0_7 });
  } else if (/(\.png)$/i.exec(path)) {
    assets = /* @__PURE__ */ Object.assign({ "/public/images/abstract-waves.png": __vite_glob_1_0, "/public/images/equipment/free-diving/bottom-weight.png": __vite_glob_1_1, "/public/images/equipment/free-diving/fins.png": __vite_glob_1_2, "/public/images/equipment/free-diving/freediving-buoy.png": __vite_glob_1_3, "/public/images/equipment/free-diving/freediving-mask.png": __vite_glob_1_4, "/public/images/equipment/free-diving/freediving-suit.png": __vite_glob_1_5, "/public/images/equipment/free-diving/lanyard.png": __vite_glob_1_6, "/public/images/equipment/free-diving/long-fins.png": __vite_glob_1_7, "/public/images/equipment/free-diving/lope.png": __vite_glob_1_8, "/public/images/equipment/free-diving/snorkel.png": __vite_glob_1_9, "/public/images/equipment/free-diving/weight-belt.png": __vite_glob_1_10, "/public/images/equipment/free-diving/weight.png": __vite_glob_1_11, "/public/images/equipment/scuba-diving/closed-heel-fins.png": __vite_glob_1_12, "/public/images/equipment/scuba-diving/diving-suit.png": __vite_glob_1_13, "/public/images/equipment/scuba-diving/open-heeled-fins.png": __vite_glob_1_14, "/public/images/equipment/snorkeling/buoy.png": __vite_glob_1_15, "/public/images/equipment/snorkeling/closed-heel-fins.png": __vite_glob_1_16, "/public/images/equipment/snorkeling/diving-suit.png": __vite_glob_1_17, "/public/images/equipment/snorkeling/kick-board.png": __vite_glob_1_18, "/public/images/equipment/snorkeling/life-buoy.png": __vite_glob_1_19, "/public/images/equipment/snorkeling/life-jacket.png": __vite_glob_1_20, "/public/images/equipment/snorkeling/mask.png": __vite_glob_1_21, "/public/images/equipment/snorkeling/open-heeled-fins.png": __vite_glob_1_22, "/public/images/equipment/snorkeling/parasol.png": __vite_glob_1_23, "/public/images/equipment/snorkeling/rashguard-and-wetsuit.png": __vite_glob_1_24, "/public/images/equipment/snorkeling/rashguard-bottom.png": __vite_glob_1_25, "/public/images/equipment/snorkeling/rashguard-top.png": __vite_glob_1_26, "/public/images/equipment/snorkeling/rashguard.png": __vite_glob_1_27, "/public/images/equipment/snorkeling/snorkel.png": __vite_glob_1_28, "/public/images/equipment/snorkeling/swim-goggles.png": __vite_glob_1_29, "/public/images/equipment/snorkeling/water-shoes.png": __vite_glob_1_30, "/public/images/equipment/snorkeling/wetsuit-bottom.png": __vite_glob_1_31, "/public/images/equipment/snorkeling/wetsuit-top.png": __vite_glob_1_32, "/public/images/hero-bg.png": __vite_glob_1_33, "/public/images/logo-gray.png": __vite_glob_1_34, "/public/images/logo-new.png": __vite_glob_1_35, "/public/images/logo.png": __vite_glob_1_36 });
  } else {
    assets = /* @__PURE__ */ Object.assign({ "/public/images/about-1.jpg": __vite_glob_2_0, "/public/images/course/aow.jpg": __vite_glob_2_1, "/public/images/course/fundive-2.jpg": __vite_glob_2_2, "/public/images/course/fundive.jpg": __vite_glob_2_3, "/public/images/course/longdong-view-1.jpg": __vite_glob_2_4, "/public/images/course/longdong-view-2.jpg": __vite_glob_2_5, "/public/images/course/longdong-view-3.jpg": __vite_glob_2_6, "/public/images/course/ow.jpg": __vite_glob_2_7, "/public/images/course/rock-climbing.jpg": __vite_glob_2_8, "/public/images/course/scuba-experience.jpg": __vite_glob_2_9, "/public/images/course/snorkeling-2.jpg": __vite_glob_2_10, "/public/images/course/snorkeling.jpg": __vite_glob_2_11, "/public/images/course/sup-2.jpg": __vite_glob_2_12, "/public/images/course/sup.jpg": __vite_glob_2_13, "/public/images/equipment/scuba-diving/bcd.jpg": __vite_glob_2_14, "/public/images/equipment/scuba-diving/regulator.jpg": __vite_glob_2_15, "/public/images/proposal-2.jpg": __vite_glob_2_16 });
  }
  const fileName = filename(path);
  const images = Object.fromEntries(
    Object.entries(assets).map(([key, value]) => [
      filename(key),
      value.default
    ])
  );
  return images[fileName].replace("_nuxt/public", "");
}

export { useAssets as u };
//# sourceMappingURL=useAssets-ef8ea7b3.mjs.map
