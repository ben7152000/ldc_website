import { defineNuxtConfig } from 'nuxt/config'
import { getVersion } from './src/shared/get-version'

export default defineNuxtConfig({
  ssr: true,
  components: true,
  build: {
    mode: 'production',
    splitChunks: {
      layouts: true,
      pages: true,
      commons: true,
    },
  },
  image: {
    formats: ['webp'],
  },
  render: {
    etag: {
      weak: false,
    },
    static: {
      maxAge: 60 * 60 * 24 * 7,
    },
  },
  srcDir: 'src/',
  modules: [
    '@nuxtjs/tailwindcss',
    'nuxt-icon',
    '@nuxt/content',
    'nuxt-swiper',
    'nuxt-gtag',
    '@nuxt/image',
  ],
  css: ['~/assets/fonts/noto-sans-tc/notosanstc.css', '~/assets/css/tailwind.css'],
  app: {
    pageTransition: { name: 'page', mode: 'out-in' },
    head: {
      meta: [
        { charset: 'utf-8' },
        { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      ],
      link: [
        { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
        {
          rel: 'stylesheet',
          href: 'https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css',
        },
      ],
      htmlAttrs: {
        'data-version': getVersion(),
      },
      script: [
        {
          type: 'text/javascript',
          hid: 'fb-customer-chat',
          body: true,
          innerHTML: `
      var chatbox = document.getElementById('fb-customer-chat');
      chatbox.setAttribute("page_id", 342245025811970);
      chatbox.setAttribute("theme_color", "#34b1b1");
      chatbox.setAttribute("locale", "zh_TW");
      chatbox.setAttribute("logged_out_greeting", "嗨~有問題可以私訊我喔！");
      chatbox.setAttribute("logged_in_greeting", "您好，很高興為您服務");

      window.fbAsyncInit = function() {
        FB.init({
          xfbml            : true,
          version          : 'v11.0'
        });
      };

      (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/zh_TW/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));`,
        },
      ],
    },
  },
  content: {
    watch: {
      ws: false,
    },
  },
  routeRules: {
    '/equipment': { redirect: '/equipment/renting' },
  },
  gtag: {
    id: 'G-Y2E3GN9MDE',
  },
})
